﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Brick : MonoBehaviour
{

    public Color[] colors = {Color.red, new Color32(255,153,51,255), Color.yellow, Color.green, Color.blue, new Color32(255,0,255,255)}; 
    [Range(1, 6)]
    public int startingBrickHealth;   
    public int powerupChance;
	private int _currentBrickHealth;
    private SpriteRenderer brickColor;
    public Sprite powerUpSpriteMulti;
    public Sprite powerUpSpriteGrowPaddle;
    public Sprite powerUpSpriteShrinkPaddle;
    public Sprite powerUpSpriteSuperBall;

    public FloatVariable bricksInScene;  

        public enum PowerupType {
        None,
        PaddleGrow,
        PaddleShrink,
        SuperBall,
        Multiball
    } 

    public PowerupType powerupType = 0;

    void Awake(){
        
    }


    // Start is called before the first frame update
    void Start(){ 
        
        if(Random.Range(0,100) < powerupChance && powerupType == PowerupType.None){
            int number = Random.Range(1,5);
            powerupType = (PowerupType)number;
        }
        if(startingBrickHealth != 0){
            _currentBrickHealth = startingBrickHealth;
        } else{
            _currentBrickHealth = 1;
        }        
        brickColor = GetComponent<SpriteRenderer>(); 
        switch (powerupType){
            case PowerupType.Multiball: 
                brickColor.sprite = powerUpSpriteMulti;
                _currentBrickHealth = 1;                       
                break;
            case PowerupType.PaddleGrow:
                brickColor.sprite = powerUpSpriteGrowPaddle;
                _currentBrickHealth = 1;
                break;
            case PowerupType.PaddleShrink:
                _currentBrickHealth = 1;
                brickColor.sprite = powerUpSpriteShrinkPaddle;
                break;
            case PowerupType.SuperBall:            
                _currentBrickHealth = 1;
                brickColor.sprite = powerUpSpriteSuperBall;
                break;
            case PowerupType.None:                
                brickColor.color = colors[_currentBrickHealth - 1];
                bricksInScene.Value++;
                break;
        }

		
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if(other.collider.tag == "Ball"){
	        switch (powerupType){
            case PowerupType.Multiball:            
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().SpawnMultiball();
                break;
            case PowerupType.PaddleGrow:
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().ChangePaddleSize(+0.25f);
                break;
            case PowerupType.PaddleShrink:
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().ChangePaddleSize(-0.25f);
                break;
            case PowerupType.SuperBall:
                other.gameObject.GetComponent<Ball>().ActivateSuperBall();
                break;
            case PowerupType.None:                
                break;
            }            
            _currentBrickHealth--;
            if(_currentBrickHealth < 1){
				    Destroy(this.gameObject);
                    bricksInScene.Value--;
			    } else {
                    brickColor.color = colors[_currentBrickHealth - 1];
                }
		}
        
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Ball"){
	        switch (powerupType){
            case PowerupType.Multiball:            
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().SpawnMultiball();
                break;
            case PowerupType.PaddleGrow:
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().ChangePaddleSize(+0.25f);
                break;
            case PowerupType.PaddleShrink:
                GameObject.FindGameObjectWithTag("Paddle").GetComponent<Paddle>().ChangePaddleSize(-0.25f);
                break;
            case PowerupType.SuperBall:
                other.gameObject.GetComponent<Ball>().ActivateSuperBall();
                break;
            case PowerupType.None:                
                break;
            }  
        }
    }

    public void DestroyBrick(){
        bricksInScene.Value--;
        Destroy(this.gameObject);
    }

    


}
