﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{


    public FloatVariable bricksInScene;
    public GameObject brickSpawnPoint;
    private bool gameOver = false;
    public Paddle paddle;
    public GameObject[] levelPrefabs;
    public FloatVariable playerLives;
    public float startingLives;

    public UnityEvent PlayerDeath;
    public UnityEvent LevelComplete;

    public GameObject textUIPanel;
    public GameObject gameCompleteUIPanel;

    public Text ballsLeftText;
    public Text levelOverText;
    public Text bricksleftText;
    public FloatVariable ballsInScene;
    [SerializeField] private GameObject currentLevelBricks;
    private bool resetting = false;
    // Start is called before the first frame update
    void Start()
    {
        playerLives.Value = startingLives;
        SetTextPanelActive(true);
        currentLevelBricks = SpawnBricks();
        ballsInScene.Value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerLives.Value == 0)
        {
            PlayerDeath.Invoke();
        }

        if (bricksInScene.Value < 1 && !resetting)
        {
            LevelComplete.Invoke();
        }

        if (bricksInScene.Value > 1)
        {
            resetting = false;
        }
    }

    public void SetTextPanelActive(bool active)
    {
        textUIPanel.SetActive(active);
        ballsLeftText.text = "Balls left: " + playerLives.Value;
    }

    public void SetLevelOverPanel(string overMessage)
    {
        gameCompleteUIPanel.SetActive(true);
        bricksleftText.text = "Bricks left: " + bricksInScene.Value;
        levelOverText.text = overMessage;
    }

    public void LoadMenu()
    {
        SceneManager.LoadScene("Menu");
    }

    public GameObject SpawnBricks()
    {
        return Instantiate(levelPrefabs[Random.Range(0, levelPrefabs.Length)], brickSpawnPoint.transform.position, Quaternion.identity);
    }

    public void ResetButtonClick()
    {
        resetting = true;
        Debug.Log("resetting");
        Destroy(currentLevelBricks.gameObject);
        ballsInScene.Value = 0;
        bricksInScene.Value = 0;
        playerLives.Value = startingLives;
        paddle.SpawnBall();
        paddle.canMove = true;
        currentLevelBricks = SpawnBricks();
        gameCompleteUIPanel.SetActive(false);
    }

}
