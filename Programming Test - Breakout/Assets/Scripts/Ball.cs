﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Ball : MonoBehaviour
{
    public UnityEvent playerLostHP;
    public UnityEvent ballLaunched;
    public FloatVariable ballsInScene;

    public bool canMove;
    public bool isSuperBall;
    public float superBallLength = 10f;
    public Sprite superBallSprite;
    public Sprite ballSprite;
    private Rigidbody2D rigidbodyBall;
    public float startingBallSpeed = 10f;
    [SerializeField] private bool _ballFired = false;
    [SerializeField] private float _currentBallSpeed;
    public float ballSpeedBounceMultiplier = 1.01f;
    public FloatVariable playerHealth;
    [SerializeField] private float lastTimeSinceCollision;
    [SerializeField] private float ballStuckTimer = 8f;


    // Start is called before the first frame update
    void Start()
    {
        canMove = false;
        rigidbodyBall = GetComponent<Rigidbody2D>();
        _currentBallSpeed = startingBallSpeed;
    }

    // Update is called once per frame
    void Update()
    {

        if(rigidbodyBall.velocity != Vector2.zero){
            _ballFired = true;
        }
        if (!_ballFired && ballsInScene.Value == 1)
        {
            transform.position = transform.parent.transform.position;
        }
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.W) && !_ballFired)
        {
            canMove = true;
            _ballFired = true;
            transform.SetParent(null);
            float horizontalSpeed = Input.GetAxis("Horizontal") * _currentBallSpeed;
            rigidbodyBall.AddForce(new Vector2(horizontalSpeed, _currentBallSpeed));
            ballLaunched.Invoke();
        }
        if (_ballFired)
        {
            lastTimeSinceCollision += Time.deltaTime;
        }

        if(Input.GetKeyDown(KeyCode.P)){
            ActivateSuperBall();
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {

        if (other.collider.tag == "Brick" || other.collider.tag == "Floor")
        {
            lastTimeSinceCollision = 0;
        }

        if (other.collider.tag == "Wall" && lastTimeSinceCollision > ballStuckTimer)
        {
            lastTimeSinceCollision = 0;
            rigidbodyBall.velocity = new Vector2(0.3f * -(rigidbodyBall.velocity.x), 0.5f).normalized * rigidbodyBall.velocity.magnitude;
        }

        if (other.collider.tag == "Paddle")
        {
            rigidbodyBall.velocity = new Vector2(rigidbodyBall.velocity.x + Input.GetAxis("Horizontal"), rigidbodyBall.velocity.y);
        }

        rigidbodyBall.velocity *= ballSpeedBounceMultiplier;

        if (other.collider.tag == "Floor" && ballsInScene.Value == 1)
        {
            _currentBallSpeed = startingBallSpeed;
            _ballFired = false;
            playerHealth.Value--;
            Destroy(this.gameObject);
            playerLostHP.Invoke();
        } else if(other.collider.tag == "Floor" && ballsInScene.Value > 1){
            ballsInScene.Value--;
            Destroy(this.gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == "Brick" && isSuperBall){
            other.GetComponent<Brick>().DestroyBrick();
        }                    
    }

    public void ActivateSuperBall(){
        StartCoroutine(superBall());
    }

    IEnumerator superBall(){
        GetComponent<SpriteRenderer>().sprite = superBallSprite;
        isSuperBall = true;
        yield return new WaitForSeconds(superBallLength);
        GetComponent<SpriteRenderer>().sprite = ballSprite;
        isSuperBall = false;
    }

}
