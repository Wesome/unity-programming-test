﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Paddle : MonoBehaviour
{

    [SerializeField] private float minPaddleSize = 0.5f;
    [SerializeField] private float maxPaddleSize = 1.5f;
    public FloatVariable ballsInScene;
    public bool canMove;
    public FloatVariable playerLives;
    public GameObject ballPrefab;
    public GameObject ballSpawnPosition;

    private GameObject ball;

    [SerializeField] private float _paddleSpeed = 1f;
    private Rigidbody2D rigidbodyPaddle;


    // Start is called before the first frame update
    void Start()
    {
        rigidbodyPaddle = GetComponent<Rigidbody2D>();
        canMove = true;
        SpawnBall();        
    }

    // Update is called once per frame
    void Update()
    {        
        if (canMove)
        {
            float movement = Input.GetAxis("Horizontal") * _paddleSpeed;
            rigidbodyPaddle.velocity = new Vector2(movement, 0);
            if (Input.GetAxis("Horizontal") == 0)
            {
                rigidbodyPaddle.velocity = Vector2.zero;
            }
        }     
        if(Input.GetKeyDown(KeyCode.O)){
            SpawnMultiball();
        }
    }

    public void SpawnBall(){  
        if(playerLives.Value > 0){
            ball = Instantiate(ballPrefab, ballSpawnPosition.transform);
            transform.localScale = Vector3.one;
            ballsInScene.Value++;
        }        
    }
    public void DestroyBall()
    {
        Destroy(ball.gameObject);        
    }

    public void ChangePaddleSize(float size){
        if(canMove){
            transform.localScale  = new Vector3(Mathf.Clamp(transform.localScale.x + size, minPaddleSize, maxPaddleSize), transform.localScale.y, transform.localScale.z);
        }        
    }

    public void SpawnMultiball(){
        StartCoroutine(MultiBall());
    }    

    IEnumerator MultiBall(){
        GameObject ball2 = Instantiate(ballPrefab, ballSpawnPosition.transform.position, Quaternion.identity);
        ball2.GetComponent<Rigidbody2D>().velocity = Vector2.one * 2f;
        ballsInScene.Value++;
        yield return new WaitForSeconds(0.5f);
        GameObject ball3 = Instantiate(ballPrefab, ballSpawnPosition.transform.position, Quaternion.identity);
        ball3.GetComponent<Rigidbody2D>().velocity = new Vector2(-1,1) * 2f;        
        ballsInScene.Value++;
        yield return new WaitForSeconds(1f); 
    }
}
